# Échelle de Thurstone

Thurstone est une application d'enquêtes qui permet d'avoir des résultats sur des échelles prédéfinies.

## Installation
* Récupérer les fichiers du projet : `git clone https://git.renater.fr/thurstone.git`
* Importer la structure sql de la base de données depuis le répertoire `distribution`.
	- Exemple sous Linux, `cat distribution/install.sql | mysql -h localhost -u user -p db_thurstone`
* Lancer l'installation via compsoser
	- Exemple sous Linux, `wget https://getcomposer.org/installer -O - | php
php composer.phar install`
* Copier à la racine du projet le fichier de configuration par défaut, le compléter et enfin supprimer la 3ème ligne contenant l'instruction `die()`
	- Exemple sous Linux, `cp distribution/config.php .`

## Plus d'informations

### Page officielle du projet
https://sourcesup.renater.fr/projects/thurstone/

### Licence du projet
Ce projet est open-source. Il est distribué sous licence MIT (cf le fichier LICENSE).

### Auteurs du projet
- Yoran Poupon, pour la DSI de l'Universite Rennes 2
- Ronan Bouilly, dans le cadre de son stage à la DSI de l'Universite Rennes 2
