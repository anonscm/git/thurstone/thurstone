<?php

echo '<nav id="navigation" role="navigation">'.
    '<h1 class="sr-only">Menu</h1>'.
    '<ul id="navigation-ul">';
foreach ($navigation as $nav) {
    if ($nav->url !== null) {
        echo '<li class="navigation-li"><a href="'.$nav->url.'">'.$nav->title.'</a></li>';
    } else {
        echo '<li class="navigation-li">'.$nav->title.'</li>';
    }
}
echo '</ul></nav>';
