<?php

require __DIR__.'/../../config.php';
require __DIR__.'/../../include/connect_mysql.inc.php';
//include("../include/forceAuthentication.inc.php");

if (isset($_GET["id"])) {
    $id = (int) $_GET["id"];
    $sql = "SELECT name FROM survey WHERE id = ".$id." AND archived = 0;";
    $result = $database->query($sql) or die(print_r($database->errorInfo()));
    if ($data = $result->fetch()) {
        $name = $data["name"];
    }
    $result->closeCursor();
}
if (!isset($name)) {
    $database = null;
    die();
}

function printLine($line)
{
    $separator = false;
    foreach ($line as $value) {
        if ($separator) {
            echo ",";
        }
        echo $value;
        $separator = true;
    }
    echo "\n";
}

$filename = utf8_decode($id."-".$name.".csv");
header('Content-type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename="'.$filename.'"');

$sql = "SELECT id, label FROM item WHERE survey_id = ".$id." ORDER BY id;";
$result = $database->query($sql) or die(print_r($database->errorInfo()));
$items = array();
while ($data = $result->fetch()) {
    $label = str_replace("\"", "\"\"", $data["label"]);
    $items[$data["id"]] = "\"".$label."\"";
}
$result->closeCursor();

printLine($items);

$sql = "SELECT item_id, value FROM result WHERE survey_id = ".$id." ORDER BY group_id, item_id;";
$result = $database->query($sql) or die(print_r($database->errorInfo()));
$data = $result->fetch();
while ($data) {
    $line = array();
    for ($i = 0; $i < count($items); $i++) {
        $line[$data["item_id"]] = $data["value"];
        $data = $result->fetch();
    }
    printLine($line);
}
$result->closeCursor();

$database = null;
