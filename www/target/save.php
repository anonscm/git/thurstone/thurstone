<?php
session_start();

require __DIR__.'/../../config.php';
require __DIR__.'/../../include/connect_mysql.inc.php';

try {
    $database->beginTransaction();
    $result = $database->query("SELECT COALESCE(MAX(group_id) + 1,0) AS next FROM result;") or die(print_r($database->errorInfo()));
    
    
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        $group_id =$row["next"];
    }
    $req = $database->prepare("INSERT INTO result (group_id, survey_id, item_id, value) VALUES (:group_id, :survey_id, :item_id, :value);");
    $survey_id = $_SESSION["id"];
    $results = $_GET["p"];
    foreach ($results as $line) {
        list($item_id, $table, $row, $value) = explode("_", $line);
        $req->execute(array(
            "group_id" => $group_id,
            "survey_id" => $survey_id,
            "item_id" => $item_id,
            "value" => $value + 1
        ));
    }
    $database->commit();
} catch (Exception $e) {
    $database->rollBack();
    echo "Failed: " . $e->getMessage();
}
$database = null;
$_SESSION["step"] = 4;
header("location: ../index.php?id=".$_SESSION["id"]);
exit(0);
