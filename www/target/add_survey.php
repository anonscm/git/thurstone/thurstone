<?php
session_start();

require __DIR__.'/../../config.php';
require __DIR__.'/../../include/connect_mysql.inc.php';
require __DIR__.'/../../include/forceAuthentication.inc.php';

$is_valid = true;
$name           = $_POST["name"]        ? $_POST["name"]        : "";
$instruction    = $_POST["instruction"]     ? $_POST["instruction"]     : "";
$contact        = $_POST["contact"]         ? $_POST["contact"]         : "";
$scale          = $_POST["scale"]       ? $_POST["scale"]       : 11;
$random         = intval(isset($_POST["random"]));
$begin          = $_POST["begin"]       ? $_POST["begin"]       : null;
$end            = $_POST["end"]             ? $_POST["end"]             : null;
$items          = $_POST["items"]       ? $_POST["items"]       : null;

if ($name == "" || $instruction == "" || $contact == "" || $scale < 2 || $items == null) {
    $is_valid = false;
} else {
    $items_count = count($items);

    if ($items_count == 0) {
        $is_valid = false;
    } else {
        for ($i = 0; $i < $items_count; $i++) {
            if ($items[$i] == "") {
                if ($i = $items_count-1) {
                    unset($items[$i]);
                } else {
                    $is_valid = false;
                }
            }
        }
    }
}

$_SESSION["last_id_added"] = -1;
if ($is_valid) {
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Core.Encoding', 'UTF-8'); // replace with your encoding
    $config->set('HTML.Doctype', 'HTML 4.01 Strict'); // replace with your doctype
    $purifier = new HTMLPurifier($config);

    $req = $database->prepare("INSERT INTO survey (owner, name, instruction, contact, scale, random, begin, end, closed, archived) VALUES (:owner, :name, :instruction, :contact, :scale, :random, :begin, :end, 0, 0);");
    $parameters = array(
        "owner" => phpCAS::getUser(),
        "name" => htmlentities($name, ENT_QUOTES, 'UTF-8'),
        "instruction" => $purifier->purify($instruction),
        "contact" => htmlentities($contact, ENT_QUOTES, 'UTF-8'),
        "scale" => htmlentities($scale, ENT_QUOTES, 'UTF-8'),
        "random" => htmlentities($random, ENT_QUOTES, 'UTF-8'),
        "begin" => htmlentities($begin, ENT_QUOTES, 'UTF-8'),
        "end" => htmlentities($end, ENT_QUOTES, 'UTF-8')
        );
    $execution = $req->execute($parameters);

    if ($execution === false) {
        error_log(sprintf('SQL Error: %s line %s : %s', __FILE__, __LINE__, json_encode($req->errorInfo())));
        error_log(sprintf('SQL Error: %s line %s : %s', __FILE__, __LINE__, json_encode($parameters)));
    } else {
        $survey_id = $database->lastInsertId();
        for ($position = 0; $position < $items_count; $position++) {
            $req = $database->prepare("INSERT INTO item (survey_id, label, position) VALUES (:survey_id, :label, :position);");
            $parameters = array(
                "survey_id" => $survey_id,
                "label" => $purifier->purify($items[$position]),
                "position" => $position
                );
            $execution = $req->execute($parameters);

            if ($execution === false) {
                error_log(sprintf('SQL Error: %s line %s : %s', __FILE__, __LINE__, json_encode($req->errorInfo())));
                error_log(sprintf('SQL Error: %s line %s : %s', __FILE__, __LINE__, json_encode($parameters)));
            }
        }
        $_SESSION["last_id_added"] = $survey_id;
    }
}

$database = null;
header("location: ../admin.php?page=create");
exit(0);
