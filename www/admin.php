<?php

$configurationfile = __DIR__.'/../config.php';

if (!is_file($configurationfile)) {
    die('Fichier de configuration manquant.');
}

require __DIR__.'/../config.php';

require __DIR__.'/../include/connect_mysql.inc.php';
require __DIR__.'/../include/forceAuthentication.inc.php';

require __DIR__.'/../include/header.inc.php';

require __DIR__.'/../classes/thurstone/helpers.php';

$navigation = array(
    new Menu('Thurstone', $cthurstone['site_url'].$cthurstone['base_url'].'/index.php'),
    new Menu('administration', $cthurstone['site_url'].$cthurstone['base_url'].'/admin.php')
);

$SCRIPTS = array();

// DEFAULT_PAGE = 'list'
if (!isset($_GET['page'])) {
    $_GET['page'] = 'list';
}

switch ($_GET['page']) {
    case 'create':
        $page = 'create';
        $navigation[] = new Menu('création d\'une enquête');
        $SCRIPTS[] = new Script($cthurstone['site_url'].$cthurstone['base_url'].'/script/tinymce/tinymce.min.js');
        $SCRIPTS[] = new Script($cthurstone['site_url'].$cthurstone['base_url'].'/script/tinymce.config.js');
        break;
    case 'edit':
        $page = 'edit';
        $navigation[] = new Menu('édition d\'une enquête');
        break;
    case 'delete':
        $page = 'delete';
        $navigation[] = new Menu('suppression d\'une enquête');
        break;
    default:
        $page = 'list';
        $navigation[] = new Menu('liste des enquêtes');
}

require __DIR__.'/pages/'.$page.'.inc.php';
require __DIR__.'/../include/footer.inc.php';
