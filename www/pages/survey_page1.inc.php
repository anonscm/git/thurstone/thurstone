<div id="Page1">
	<p id="Instructions">
		<strong>Bonjour,<br />
Cette enquête va nous permettre de classer des énoncés les uns par rapport aux autres, pour déterminer ceux qui sont défavorables et ceux qui sont favorables à notre objet d'étude.<br />
Votre tâche consiste à classer ces énoncés les uns par rapport aux autres sur une échelle allant de tout à fait défavorable à tout à fait favorable.<br /></strong>
À titre d'exemple et pour tester l'application, nous vous proposons de vous exercer sur l'échelle ci-dessous, sur laquelle vous devez situer des dégradés de couleurs.<br />
<strong>Vous ne devez pas indiquer votre avis ! Vous devez seulement comparer chaque dégradé et indiquer ceux qui sont plutôt verts et ceux qui sont plutôt bleus.</strong><br />
Votre rôle consiste donc à glisser tous les dégradés se trouvant dans la colonne "Item" (à gauche) dans l’une des colonnes du tableau. Si un dégradé est plutôt vert vous le placerez plus près du 1. Si un dégradé est plutôt bleu, vous le placerez plus près du 11. Vous pouvez moduler ce classement grâce aux colonnes situées entre ces deux extrêmes.<br />
Toutes les colonnes ne doivent pas nécessairement être utilisées. Deux énoncés (ou plus) peuvent ainsi se retrouver dans une même colonne.<br />
<strong>Vous pouvez déplacer les dégradés (même ceux déjà placés) autant de fois que nécessaire.</strong>
	</p>
	<div id="Drag">
		<div id="Left">
			<table>
				<thead>
					<tr>
						<th class="mark items">Items</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td id="Items">
							<?php
                            echo "<div class=\"drag\" style=\"background: rgb(0,255,102); color: #000000;\">Dégradé n°1</div>";
                            echo "<div class=\"drag\" style=\"background: rgb(0,102,255); color: #000000;\">Dégradé n°2</div>";
                            for ($i = 3; $i <= 10; $i++) {
                                $value = rand(0, 511);
                                $r = 0;
                                if ($value < 256) {
                                    $g = 255;
                                    $b = $value;
                                } else {
                                    $g = $value - 256;
                                    $b = 255;
                                }
                                echo "<div class=\"drag\" style=\"background: rgb(".$r.",".$g.",".$b."); color: #000000;\">Dégradé n°".$i."</div>";
                            }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="Right">
            <table>
                <thead>
                    <tr>
                        <?php
                        echo "<th class=\"mark\" style=\"background: #00FF00; color: #FFFFFF;\">1<br />Vert</th>";
                        for ($i = 2; $i < 11; $i++) {
                            echo "<th class=\"mark\">$i</th>";
                        }
                        echo "<th class=\"mark\" style=\"background: #0000FF; color: #FFFFFF;\">11<br />Bleu</th>";
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php
                        for ($i = 1; $i <= 11; $i++) {
                            echo "<td></td>";
                        }
                        ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <p><input id="Next" class="button submit" type="button" value="Continuer" onclick="next();" /></p>
</div>
