<script type="text/javascript" src="script/create.js"></script>
<h2>Edition de l'enquete</h2>
<?php

require __DIR__.'/../../html/commons/navigation.php';

if (isset($_SESSION["last_id_added"])) {
    if ($_SESSION["last_id_added"] == -1) {
        echo "<p class=\"content\" style=\"color: red;\"><strong>Les données avec un * doivent être saisies.</strong></p>";
    } else {
        $url = sprintf('%s%s/index.php?id=%s', $cthurstone['site_url'], $cthurstone['base_url'], $_SESSION["last_id_added"]);
        echo "<p class=\"content\" style=\"color: green;\"><strong>Enquête n°".$_SESSION["last_id_added"]." éditée avec succès.</strong></p>";
        echo "<p class=\"content\" style=\"margin: .5em 10px 2em 10px;\">Lien d'accès à l'enquête : <a href=\"".$url."\">".$url."</a></p>";
    }
    $_SESSION["last_id_added"] = null;
}

require __DIR__.'/../../config.php';
require __DIR__.'/../../include/connect_mysql.inc.php';

$idN= $_GET["id"];
$sqlN="SELECT * FROM survey where id = :id";
$query = $database->prepare($sqlN);
$query->execute(array(':id' => $idN));
$data = $query->fetch(PDO::FETCH_NUM);

?>
<form id="Create" class="content" method="post" action="target/add_survey.php">
    <fieldset id="SurveyFieldset">
        <legend>Enquête</legend>
        <p><label for="name">Titre (ou objet) de l'échelle * : </label><input type="text" id="name" name="name" placeholder="Nom de l'enquête" value="<?php echo $data[2];?>" maxlength="256" required="required" /></p>
        <p><label for="contact">Contact * : </label><input type="email" id="contact" name="contact" placeholder="votre-adresse@domaine.fr" maxlength="64" required="required" value="<?php echo $user->mail; ?>" /></p>
        <p><label for="scale">Nombre de modalités de l'échelle * : </label><input type="number" id="scale" name="scale" placeholder="De 1 à ..." value="<?php echo $data[5];?>" required="required" /></p>
        <p><label for="random">Afficher les items dans un ordre aléatoire : </label><input type="checkbox" id="random" name="random"<?php if (!empty($data[6])) { echo ' checked="checked"';} ?> /></p>
        <p><label for="begin">Début de l'enquête : <!--</label><input type="text" id="begin" name="begin" placeholder="aaaa-mm-jj" />--><?php echo $data[7];?></p>
        <p><label for="end">Fin de l'enquête : <!--</label><input type="text" id="end" name="end" placeholder="aaaa-mm-jj" />--><?php echo $data[8];?></p>
        <p class="noMargin"><label for="instruction">Consignes * : </label>
        <textarea name="instruction" id="instruction" class="noMargin" required="required">
Classez tous les énoncés, en fonction de s'ils vous semblent plutôt favorables ou plutôt défavorables à [OBJET A PRÉCISER].
Vous ne devez pas indiquer votre avis ! Vous devez seulement comparer chaque énoncé les uns aux autres et déterminer ceux qui sont les plus favorables et ceux qui sont les moins favorables à [OBJET À PRÉCISER].
Votre rôle consiste à glisser tous les énoncés se trouvant dans la colonne "Item" (à gauche) dans l'une des colonnes du tableau.
Si un énoncé est plutôt défavorable à [OBJET À PRÉCISER] vous le placerez plus près du 1. Si un énoncé est plutôt favorable à [OBJET À PRÉCISER], vous le placerez plus près du [11 ou valeur maximale]. Vous pouvez moduler ce classement grâce aux colonnes situées entre ces deux extrêmes.
Tous les énoncés doivent être placés dans le tableau, Toutes les colonnes ne doivent pas nécessairement être utilisées : deux énoncés (ou plus) peuvent ainsi se retrouver dans une même colonne.
Vous pouvez déplacer les énoncés (même ceux déjà placés) autant de fois que nécessaire.
</textarea></p>
    </fieldset>
    <!--<fieldset id="ItemsFieldset">
        <legend>Items</legend>
        <ul id="ItemsList">
        </ul>
        <script type="text/javascript">addItem();</script>
        <p class="noMargin"><input id="AddItem" type="button" class="button add" value="Ajouter un item" onclick="addItem()" title="Ajouter un item" /></p>
    </fieldset>

    
    <p><input type="submit" class="button send" value="Éditer" /></p>-->

</form>
<form id="modif" class="content" method=post action="modifier.inc.php" >
        <h3>Liste des items :</h3>
    <p><br/>
    <?php
    
    // apparition et modification des items déjà créé
    
    $id= $_GET['id'];

        
    $sql= 'SELECT * FROM item where survey_id = :id';
    $query = $database->prepare($sql);
    $query->execute(array(':id' => $id));

    while ($row = $query->fetch(PDO::FETCH_NUM)) {
        echo '<h4>'.$row[2].'</h4>';
        echo '<a href="'.$cthurstone['base_url'].'/pages/modifier.inc.php?item_id='.$row[0].' "name="label">modifier</a>';
        echo '<br/>';
    }

    ?>
    </p>
    </form>
