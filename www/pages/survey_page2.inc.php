<div id="Page2">
	<?php
    echo "<p id=\"Instructions\">".$instruction."</p>";
    $min_scale = 1;
    $max_scale = $scale;
    ?>
    <div id="Drag">
        <div id="Left">
            <table>
                <thead>
                    <tr>
                        <th class="mark items">Items</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="Items">
                            <?php
                            $sql = "SELECT id, label FROM item WHERE survey_id = ".$_SESSION["id"]." ORDER BY ";
                            $sql .= $random ? "rand();" : "position, id;";
                            $result = $database->query($sql) or die(print_r($database->errorInfo()));
                            while ($data = $result->fetch()) {
                                echo "<div id=\"".$data["id"]."\" class=\"drag\">".$data["label"]."</div>";
                            }
                            echo "</td>";
                            $result->closeCursor();
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="Right">
            <table>
                <thead>
                    <tr>
                    
                        <?php
                        echo "<th class=\"mark\">".$min_scale."<br />Tout à fait défavorable</th>";
                        for ($i = $min_scale + 1; $i < $max_scale; $i++) {
                            echo "<th class=\"mark\">$i</th>";
                        }
                        echo "<th class=\"mark\">".$max_scale."<br />Tout à fait favorable</th>";
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php
                        for ($i = $min_scale; $i <= $max_scale; $i++) {
                            echo "<td></td>";
                        }
                        ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <p id="Message">Glissez / déposez les items dans le tableau de droite.</p>
    <p><input id="Save" class="button submit" type="button" value="Envoyer" onclick="save();" /></p>
</div>
