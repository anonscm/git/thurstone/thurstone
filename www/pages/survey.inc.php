<div id="Survey" class="content">
	<h2 id="Titre"><?php echo $name; ?></h2>
    <div id="Infos">
    <p>Contact : <a href="mailto:<?php echo $contact; ?>"><?php echo $contact; ?></a></p>
    <?php
    if (!$closed) {
        if ($begin) {
            if (too_soon($begin)) {
                $closed = true;
            }
            echo "<p>Date d'ouverture : ".begin_date($begin)."</p>";
        }
        if ($end) {
            if (too_late($end)) {
                $closed = true;
            }
            echo "<p>Date de cloture : ".end_date($end)."</p>";
        }
    }
    echo "</div>";
    if ($closed) {
        echo "<div id=\"Closed\">";
        if (too_soon($begin)) {
            echo "<p>Cette enquête n'est pas encore ouverte.</p>";
        } elseif (too_late($end)) {
            echo "<p>Cette enquête est terminée.</p>";
        } else {
            echo "<p>Enquête verrouillée.</p>";
        }
        echo "</div>";
    } else {
        $_SESSION["step"] = isset($_SESSION["step"]) ? $_SESSION["step"] : 1;
        switch ($_SESSION["step"]) {
            case 1:
            default:
                include("survey_page1.inc.php");
                break;
            case 2:
                include("survey_page2.inc.php");
                break;
            case 3: // TODO
                include("survey_page3.inc.php");
                break;
            case 4:
                include("survey_page4.inc.php");
                break;
        }
    }
    ?>
</div>
