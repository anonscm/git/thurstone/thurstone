<script type="text/javascript" src="script/create.js"></script>
<h2> CREATION de l'enquete</h2>
<?php

require __DIR__.'/../../html/commons/navigation.php';

if (isset($_SESSION["last_id_added"])) {
    if ($_SESSION["last_id_added"] == -1) {
        echo "<p class=\"content\"><strong style=\"color: red;\">Les données avec un * doivent être saisies.</strong></p>";
    } else {
        $url = sprintf('%s%s/index.php?id=%s', $cthurstone['site_url'], $cthurstone['base_url'], $_SESSION["last_id_added"]);
        echo "<p class=\"content\"><strong style=\"color: green;\">Enquête n°".$_SESSION["last_id_added"]." créée avec succès.</strong></p>";
        echo "<p class=\"content\" style=\"margin: .5em 10px 2em 10px;\">Lien d'accès à l'enquête : <a href=\"".$url."\">".$url."</a></p>";
    }
    $_SESSION["last_id_added"] = null;
}
?>
<form id="Create" class="content" method="post" action="target/add_survey.php">
    <fieldset id="SurveyFieldset">
        <legend>Enquête</legend>
        <p><label for="name">Titre (ou objet) de l'échelle * : </label><input type="text" id="name" name="name" placeholder="Nom de l'enquête" maxlength="256" required="required" /></p>
        <p><label for="contact">Contact * : </label><input type="email" id="contact" name="contact" placeholder="votre-adresse@domaine.fr" maxlength="64" required="required" value="<?php echo $user->mail; ?>" /></p>
        <p><label for="scale">Nombre de modalités de l'échelle * : </label><input type="number" id="scale" name="scale" placeholder="De 1 à ..." value="11" required="required" /></p>       <p><label for="begin">Début de l'enquête : </label><input type="text" id="begin" name="begin" placeholder="aaaa-mm-jj" /></p>
        <p><label for="end">Fin de l'enquête : </label><input type="text" id="end" name="end" placeholder="aaaa-mm-jj" /></p>
        <!--<p><label for="info">Demander des information supplémentaires : </label><input type="checkbox" id="info" name="info" checked="checked" /></p>-->
        <p class="noMargin"><label for="instruction">Consignes * : </label>
        <textarea rows="100" cols="100" name="instruction" id="instruction" class="noMargin" required="required" >
<strong>Classez tous les énoncés, en fonction de s'ils vous semblent plutôt favorables ou plutôt défavorables à [OBJET A PRÉCISER].</strong><br />
Vous ne devez pas indiquer votre avis ! Vous devez seulement comparer chaque énoncé les uns aux autres et déterminer ceux qui sont les plus favorables et ceux qui sont les moins favorables à [OBJET À PRÉCISER].<br />
<strong>Votre rôle consiste à glisser tous les énoncés se trouvant dans la colonne "Item" (à gauche) dans l'une des colonnes du tableau.</strong><br />
Si un énoncé est plutôt défavorable à [OBJET À PRÉCISER] vous le placerez plus près du 1. Si un énoncé est plutôt favorable à [OBJET À PRÉCISER], vous le placerez plus près du [11 ou valeur maximale]. Vous pouvez moduler ce classement grâce aux colonnes situées entre ces deux extrêmes.<br />
Tous les énoncés doivent être placés dans le tableau, Toutes les colonnes ne doivent pas nécessairement être utilisées : deux énoncés (ou plus) peuvent ainsi se retrouver dans une même colonne.<br />
<strong>Vous pouvez déplacer les énoncés (même ceux déjà placés) autant de fois que nécessaire.</strong>
</textarea></p>
    </fieldset>
    <fieldset id="ItemsFieldset">
        <legend>Items</legend>
        <p>
        Indiquez un seul item par champ. Pour ajouter un champ, cliquez sur "Ajouter un item". Pour en supprimer un, cliquez sur X. Si vous n'avez pas choisi l'ordre aléatoire, vous pouvez modifier la position de l'item en utilisant les flèches ( ▲ pour faire monter l'item, ▼ pour le faire descendre)
<p><label for="random">Afficher les items dans un ordre aléatoire : </label><input type="checkbox" id="random" name="random" checked="checked" /></p>
        <ul id="ItemsList">
        </ul>
        <script type="text/javascript">addItem();</script>
        <p class="noMargin"><input id="AddItem" type="button" class="button add" value="Ajouter un item" onclick="addItem()" title="Ajouter un item" /></p>
    </fieldset>
    <p><input type="submit" class="button send" value="Créer" /></p>
</form>

