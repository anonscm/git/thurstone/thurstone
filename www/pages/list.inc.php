<div id="Admin" class="content" style="display: none;">
	<p>
		Login : <?php echo phpCAS::getUser(); ?><br />
        Nom et prénom : <?php echo $user->name; ?> <?php echo $user->surname; ?><br />
        Mail : <?php echo $user->mail; ?>
    </p>
</div>
<h2> ADMINISTRATION</h2>
<?php require __DIR__.'/../../html/commons/navigation.php'; ?>
<div id="List" class="content">
    <h3>Liste des études disponibles</h3>
    <table>
        <thead>
            <tr>
                <th>Intitulé</th>
                <th>Date de début</th>
                <th>Date de fin</th>
                <th>Enquête verrouillée</th>
                <th>URL</th>
                <th>Participations</th>
                <th>Exportation</th>
                <th>Supprimer</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $sql = "SELECT survey.id, survey.name, survey.begin, survey.end, COUNT(DISTINCT result.group_id) AS participation, closed";
        $sql .= " FROM survey";
        $sql .= " LEFT JOIN result ON survey.id = result.survey_id";
        $sql .= " WHERE archived = 0";
        $sql .= " AND survey.owner = \"".phpCAS::getUser()."\"";
        $sql .= " GROUP BY survey.id;";
        $sql .= " ORDER BY survey.id;";
        $result = $database->query($sql) or die(print_r($database->errorInfo()));
        while ($data = $result->fetch()) {
            ?>
            <tr>
                <th><a href="admin.php?page=edit&id=<?php echo $data["id"]; ?>"><?php echo $data["name"]; ?></a></th>
                <td><?php echo begin_date($data["begin"]); ?></td>
                <td><?php echo end_date($data["end"]); ?></td>
                <td><?php echo $data["closed"] ? "Fermée" : "Ouverte"; ?></td>
                <td><a href="
                    <?php echo $cthurstone['base_url']; ?>/index.php?id=<?php echo $data["id"]; ?>" target="_blank"> <?php echo $cthurstone['base_url']; ?>/index.php?id=<?php echo $data["id"]; ?> </a>
                </td>
                <td><?php echo $data["participation"]; ?></td>
                <td><a href="<?php echo $cthurstone['base_url']; ?>/target/extraction.php?id=<?php echo $data["id"]; ?>" target="_blank">CSV</a></td>
                <td><a href="<?php echo $cthurstone['base_url']; ?>/pages/delete.inc.php?id=<?php echo $data["id"]; ?>"> X</a></td>
            </tr>
            <?php
        }
        $result->closeCursor();
        ?>
        </tbody>
    </table>
    <p><a href="?page=create">Créer une nouvelle enquête</a></p>
    
    <p>(Si vous venez de créer une enquête, et qu'elle n'apparait pas, actualisez la page)</p>
</div>
