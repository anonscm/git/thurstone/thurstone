<div id="Presentation" class="content">
	<h2>Application Thurstone</h2>
	<p>L'application Thurstone permet de construire des échelles de mesure d'attitude (échelle de Thurstone).</p>
	<p>Les enquêteurs doivent s'identifier avec leur Compte d'Authentification Sésame (CAS) pour pouvoir créer et administrer leurs enquêtes de construction d'échelle.
Une fois l'enquête créée, ils peuvent fournir une URL aux participants pour qu'ils puissent y répondre.</p>
	<p>Les participants n'ont pas de restriction pour se connecter, et peuvent donc répondre aux enquêtes sans avoir besoin de s'identifier. 
	Néanmoins, certaines enquêtes peuvent demander quelques renseignements généraux aux participants (tel que l'age, la région, etc).</p>
	<p>Une explication sur le fonctionnement de l'échelle de Thurstone est fournie à l'utilisateur au début de chaque enquête.</p>
<p>Page d'administrateur <a href="<?php echo $cthurstone['base_url']; ?>/admin.php">ici</p>
</div>
