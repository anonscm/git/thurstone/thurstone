﻿/* enable strict mode */
"use strict";

function next() {
	if (allItemsPlaced())
		window.location.href = "target/next.php";
}

function save() {
	if (allItemsPlaced())
		window.location.href = "target/save.php?" + REDIPS.drag.save_content(1);
}

function allItemsPlaced() {
	if (document.getElementById("Items").getElementsByTagName("div").length > 0) {
		alert("Merci de classer tous les items situés de la colonne de gauche avant de valider votre essai.");
		return false;
	}
	return true;
}

window.onload = function () {
	var	survey = REDIPS.drag;
	survey.init("Drag");
	survey.drop_option = "multiple";
	survey.hover_color = "#9BB3DA";
	survey.clone_shiftKey = false;
};
