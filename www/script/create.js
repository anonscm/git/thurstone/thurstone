"use strict";

/**
 * Fonctions appellées
 */
function addItem() {
	var text = document.createElement("textarea");
	text.setAttribute("class", "item_content");
	text.setAttribute("placeholder", "Contenu de l'item *");
	text.setAttribute("name", "items[]");

	var up = document.createElement("input");
	up.setAttribute("type", "button");
	up.setAttribute("title", "Déplacer cet item vers le haut");
	up.setAttribute("class", "button move_up");
	up.setAttribute("onclick", "moveUpItem(this)");
	up.setAttribute("value", "▲");

	var down = document.createElement("input");
	down.setAttribute("type", "button");
	down.setAttribute("title", "Déplacer cet item vers le bas");
	down.setAttribute("class", "button move_down");
	down.setAttribute("onclick", "moveDownItem(this)");
	down.setAttribute("value", "▼");

	var remove = document.createElement("input");
	remove.setAttribute("type", "button");
	remove.setAttribute("title", "Supprimer cet item");
	remove.setAttribute("class", "button remove");
	remove.setAttribute("onclick", "removeItem(this)");
	remove.setAttribute("value", "X");

	var item = document.createElement("li");
	item.appendChild(text);
	item.appendChild(up);
	item.appendChild(down);
	item.appendChild(remove);
	getItemsList().appendChild(item);
}

function moveUpItem(button) {
	if (itemsNb() < 2)
		return;
	var id = getLineIdFromButton(button);
	if (id == 0)
		return;
	getItemsList().insertBefore(getLine(id), getLine(id - 1));
}

function moveDownItem(button) {
	if (itemsNb() < 2)
		return;
	var id = getLineIdFromButton(button);
	if (id + 1 == itemsNb())
		return;
	getItemsList().insertBefore(getLine(id), getLine(id + 1).nextSibling);
}

function removeItem(button) {
	if (itemsNb() < 2)
		return;
	var id = getLineIdFromButton(button);
	getItemsList().removeChild(getLine(id));
}

/**
 * Fonctions internes
 */
function getLineIdFromButton(button) {
	for (var i = 0; i < itemsNb(); i++)
		if (button.parentNode == getLine(i))
			return i;
	return null;
}

function itemsNb() {
	return getItemLines().length;
}

function getLine(id) {
	return getItemLines()[id];
}

function getItemLines() {
	return getItemsList().getElementsByTagName("li");
}

function getItemsList() {
	return document.getElementById("ItemsList");
}
