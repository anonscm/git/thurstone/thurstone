// Initialize TinyMCE with the new plugin and listbox
tinyMCE.init({
	plugins : '-example', // - tells TinyMCE to skip the loading of the plugin
	mode : "exact",
	elements : "instruction", // uniquement l'id #instruction
	theme : "modern",
	plugins: "code",
	toolbar1 : "bold italic underline | styleselect | code",
	menubar : false,
	statusbar : false,
	convert_urls : false
});
