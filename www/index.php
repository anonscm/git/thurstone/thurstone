<?php

$configurationfile = __DIR__.'/../config.php';

if (!is_file($configurationfile)) {
    die('Fichier de configuration manquant.');
}

require __DIR__.'/../config.php';

require __DIR__.'/../include/header.inc.php';

require __DIR__.'/../classes/thurstone/helpers.php';

if (isset($_GET["id"])) {
    $SCRIPTS = array();
    $SCRIPTS[] = new Script($cthurstone['site_url'].$cthurstone['base_url'].'/script/redips-drag-min.js');
    $SCRIPTS[] = new Script($cthurstone['site_url'].$cthurstone['base_url'].'/script/script.js');

    if (!isset($_SESSION["id"]) || $_SESSION["id"] != (int) $_GET["id"]) {
        $_SESSION["id"] = (int) $_GET["id"];
        $_SESSION["step"] = 1;
    }
    $sql = "SELECT name, instruction, contact, scale, random, begin, end, closed FROM survey WHERE id = ".$_SESSION["id"]." AND archived = 0;";
    $result = $database->query($sql) or die(print_r($database->errorInfo()));
    if ($data = $result->fetch()) {
        $name = $data["name"];
        $instruction = $data["instruction"];
        $contact = $data["contact"];
        $scale = $data["scale"];
        $random = $data["random"];
        $begin = $data["begin"];
        $end = $data["end"];
        $closed = $data["closed"];
        require __DIR__.'/pages/survey.inc.php';
    } else {
        require __DIR__.'/pages/error_survey.inc.php';
    }
    $result->closeCursor();
} else {
    require __DIR__.'/pages/presentation.inc.php';
}
require __DIR__.'/../include/footer.inc.php';
