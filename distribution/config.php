<?php

die('config.php is incomplete');

// Charge l'autoload de composer.
require __DIR__.'/vendor/autoload.php';

// Définis l'URL du site.
$cthurstone['site_url'] = 'https://mon-site.fr';
$cthurstone['base_url'] = '/thurstone';

// Définis les paramètres CAS.
$serveurSSO = 'cas.mon-site.fr';
$serveurSSOPort = 443;
$serveurSSORacine = '';

// Définis les identifiants de la base de données.
$db_host = '';
$db_name = '';
$db_user = '';
$db_pwd  = '';

// Définis les identifiants de l'annuaire LDAP.
$ldap_address = 'ldap.mon-site.fr';
$ldap_port = 389;
$ldap_base = 'ou=people,dc=ldap,dc=mon-site,dc=fr';
$ldap_filter = '';

$ldap_login = 'cn=USER,ou=System,dc=ldap,dc=mon-site,dc=fr';
$ldap_password  = 'PASSWORD';

// Définis le nom des champs LDAP.
$ldap_id = 'uid';
$ldap_name = 'sn';
$ldap_surname = 'givenname';
$ldap_mail = 'mail';
$ldap_type = 'eduPersonPrimaryAffiliation';
