<?php

try {
    $database = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"));
} catch (Exception $e) {
    die("Erreur : ".$e->getMessage());
}
