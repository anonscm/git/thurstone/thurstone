<?php
function too_soon($date)
{
    return new DateTime() < new DateTime($date);
}

function too_late($date)
{
    return new DateTime() > new DateTime($date);
}

function begin_date($date)
{
    if (too_soon($date)) {
        return "<strong class=\"infoDate\">".$date."</strong>";
    }
    return $date;
}

function end_date($date)
{
    if (too_late($date)) {
        return "<strong class=\"infoDate\">".$date."</strong>";
    }
    return $date;
}
