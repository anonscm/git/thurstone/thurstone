<?php
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
include("connect_mysql.inc.php");
include("functions.inc.php");
?><!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <meta name="robots" content="none" />
    <title>Échelle de Thurstone</title>
    <link rel="stylesheet" type="text/css" href="<?php echo $cthurstone['site_url'] .$cthurstone['base_url']; ?>/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $cthurstone['site_url'] .$cthurstone['base_url']; ?>/css/style.css" />
    <noscript><style type="text/css">
        .content { display: none; }
    </style></noscript>
</head>
<body>
<h1 id="Header" class="line"><a href="<?php echo $cthurstone['site_url'] .$cthurstone['base_url']; ?>/">Échelle de Thurstone</a></h1>
