<?php
include("class/User.php");
$error = null;
$user = null;

function ldap($info, $value)
{
    return $info[0][strtolower($value)][0];
}
if ($ds = ldap_connect($ldap_address, $ldap_port)) {
    if ($r = ldap_bind($ds, $ldap_login, $ldap_password)) {
        $filter = "(".$ldap_id."=".phpCAS::getUser().")";
        if (!empty($ldap_filter)) {
            $filter = "(& ".$filter.$ldap_filter.")";
        }
        if ($result = ldap_search($ds, $ldap_base, $filter, array ($ldap_name, $ldap_surname, $ldap_mail, $ldap_type, $ldap_id), 0, 0)) {
            $info = ldap_get_entries($ds, $result);
            if ($info["count"] == 1 && is_array($info[0])) {
                $user = new User(ldap($info, $ldap_name), ldap($info, $ldap_surname), ldap($info, $ldap_mail), ldap($info, $ldap_type));
            } else {
                $error = "Erreur lors de la lecture des données de \"".phpCAS::getUser()."\".";
            }
            ldap_free_result($result);
        } else {
            $error = "uid \"".phpCAS::getUser()."\" introuvable dans LDAP.";
        }
    } else {
        $error = "Impossible de se connecter à l'annuaire LDAP avec ces identifiants de connexion.";
    }
    ldap_close($ds);
} else {
    $error = "Impossible de joindre l'annuaire LDAP.";
}
if ($user == null) {
    include("../www/pages/error_ldap.inc.php");
    include("footer.inc.php");
    die();
} else {
    $sql = "SELECT id FROM user WHERE id = :id";
	$query = $database->prepare($sql);
	$query->execute(array(':id' => phpCAS::getUser()));
    $userDB = $query->fetch(PDO::FETCH_ASSOC);
    //var_dump($userDB);
    if (!$userDB["id"]) {
        $req = $database->prepare("INSERT INTO user (id, name, surname, mail) VALUES (:id, :name, :surname, :mail);");
        $req->execute(array(
                "id" => strtolower(phpCAS::getUser()),
                "name" => $user->name,
                "surname" => $user->surname,
                "mail" => $user->mail
        ));
    }
}
