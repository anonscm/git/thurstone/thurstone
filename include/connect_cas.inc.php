<?php
//include('CAS/cas.sso');

phpCAS::client(CAS_VERSION_2_0, $serveurSSO, $serveurSSOPort, $serveurSSORacine, false);
phpCAS::setLang(PHPCAS_LANG_FRENCH);
phpCAS::setNoCasServerValidation();
phpCAS::setFixedServiceURL($cthurstone['site_url'] . '/' . $cthurstone['base_url'] . '/admin.php');
phpCAS::forceAuthentication();
