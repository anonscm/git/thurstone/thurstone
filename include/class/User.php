<?php

class User
{
    public $name;
    public $surname;
    public $mail;
    public $function;

    public function __construct($name, $surname, $mail, $function)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->mail = $mail;
        $this->function = $function;
    }
}
