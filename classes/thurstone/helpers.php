<?php

/**
* class Style
*
* Class to create easily 'link' HTML tags
*
* example: '<link url="'.$css->url.'" media="'.$css->media.'" rel="'.$css->rel.'" type="text/css">'
*
*/
class Style
{
    public $url;
    public $media;
    public $rel;

    public function __construct($url, $media = 'screen', $rel = 'stylesheet')
    {
        $this->url = $url;
        $this->media = $media;
        $this->rel = $rel;
    }
}


/**
* class Script
*
* Class to create easily 'script' HTML tags
*
* example: '<script src="'.$script->src.'" type="'.$script->type.'">'
*
*/
class Script
{
    public $src;
    public $type;

    public function __construct($src, $type = 'text/javascript')
    {
        $this->src = $src;
        $this->type = $type;
    }
}


/**
  * class Menu
  *
  * Class to create easily navigation menu
  *
  */
class Menu
{
    public $title;
    public $url;
    public $selected;
    public $style_classes;

    public function __construct($title, $url = null, $selected = false, $style_classes = '')
    {
        $this->title = $title;
        $this->url = $url;
        $this->selected = $selected;
        $this->style_classes = $style_classes;
    }
}
